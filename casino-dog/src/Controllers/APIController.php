<?php
namespace Wainwright\CasinoDog\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;
use Wainwright\CasinoDog\CasinoDog;
use Wainwright\CasinoDog\Controllers\Game\SessionsHandler;
use Wainwright\CasinoDog\Controllers\Game\OperatorsController;
use Illuminate\Support\Facades\Log;
use Wainwright\CasinoDog\Traits\ApiResponseHelper;
use Wainwright\CasinoDog\Models\Gameslist;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\Facades\Cache;
use Illuminate\Pagination\Paginator;
class APIController
{
   use ApiResponseHelper;

   public function gameslist_ocb($games)
   {
    foreach($games as $game) {
        $explode_game = explode(':', $game->slug);
        if(isset($explode_game[1])) {
            $game_software = $explode_game[0];
            $game_raw = $explode_game[1];
            $img_url = 'https://cdn.softswiss.net/i/s2/'.$game_software.'/'.$game_raw.'.png';
        } else {
            $img_url = 'http://5.196.36.36:9000/thumbnail/i-9102777/'.$game->gid.'.png';
        }
        $games_list[] = array(
            'id' => $game->id,
            'api_game_id' => $game->slug,
            'provider' => $game->provider,
            'category_id' => "0",
            'section_id' => "1",
            'brand_id' => "0",
            'name' => $game->name,
            'description' => $game->name,
            'supports' => array(
                'ios' => true,
                'android' => true,
            ),
            'image_file' => 'https://cdn.softswiss.net/i/   /'.$game_software.'/'.$game_raw.'.png',
            'lobby_image_file' => "",
            'position_category' => "0",
            'position_brand' => "0",
            'widescreen' => "0",
            'is_new' => "0",
            'use_jackpot' => "0",
            'use_denomination' => "0",
            'enabled' => "1",
        );
    }
    $games = collect($games_list);
    $games_list = $games->unique();

    return $games_list;
   }

   public function gameslist_wainwright($games)
   {
    foreach($games as $game) {
        $explode_game = explode(':', $game->slug);
        if(isset($explode_game[1])) {
            $game_software = $explode_game[0];
            $game_raw = $explode_game[1];
            $img_url = 'https://wainwrighted.herokuapp.com/https://d1sc13y7hrlskd.cloudfront.net/optimized_images/landscape/'.$game_software.'/'.$game_raw.'.webp';
            if($game->provider === 'netent') {
                $img_url = 'https://wainwrighted.herokuapp.com/https://cdn.softswiss.net/i/s2/'.$game_software.'/'.$game_raw.'.png';
            }
            if($game->provider === 'playngo') {
                $img_url = 'https://wainwrighted.herokuapp.com/https://cdn.softswiss.net/i/s2/'.$game_software.'/'.$game_raw.'.png';
            }
        } else {
            $img_url = 'https://wainwrighted.herokuapp.com/https://parimatch.co.tz/service-discovery/service/pm-casino/img/tr:n-slots_game_image_desktop/Casino/eva/games/'.$game->gid.'.png';
        }

        $tags = array($game->type);

        if($game->bonusbuy === 1) {
            array_push($tags, 'bonusbuy');
        }

        if($game->jackpot === 1) {
            array_push($tags, 'jackpot');
        }

        $games_list[] = array(
            'id' => $game->gid,
            'slug' => $game->slug, //slug used in api
            'name' => $game->name,
            'provider' => $game->provider,
            'tags' => $tags,
            'img' => $img_url, // link to image
            'status' => 'active',
            'created_at' => $game->created_at ?? now(),
            'updated_at' => $game->updated_at ?? now(),
        );
    }
    $games = collect($games_list);
    $games_list = $games->unique();

    return $games_list;
   }

    public function providerslist_wainwright($providers, $count) {
        if($count < 2) {
            $games_count = Gameslist::where('provider', $providers[0]['slug'])->count();
            $providerslist = array(
                'id' => $providers[0]['slug'],
                'slug' => $providers[0]['slug'],
                'name' => ucfirst($providers[0]['name']),
                'parent' => NULL,
                'eligible_games' => $games_count,
                'icon' => 'ResponsiveIcon',
                'provider' => $providers[0]['provider'],
                'created_at' => now(),
                'updated_at' => now(),
            );
        } else {
        foreach($providers as $provider) {
            $games_count = Gameslist::where('provider', $provider['slug'])->count();
            $providerslist[] = array(
                'id' => $provider['slug'],
                'slug' => $provider['slug'],
                'name' => ucfirst($provider['slug']),
                'parent' => NULL,
                'eligible_games' => $games_count,
                'icon' => 'ResponsiveIcon',
                'provider' => $provider['slug'],
                'created_at' => now(),
                'updated_at' => now(),
            );
        }
        }


        return $providerslist;
    }

public function game_descriptions() {
    $cache_length = 300; // 300 seconds = 5 minutes

    if($cache_length === 0) {
        $game_desc = file_get_contents(__DIR__.'../../game_descriptions.json');
    }
    $game_desc = Cache::remember('gameDescriptions', 300, function () {
        return file_get_contents(__DIR__.'/../../game_descriptions.json');
    });
    $g2 = json_decode($game_desc, true);

    return $g2;
}

   public function gameslist_trimmed($games)
   {
    foreach($games as $game) {
        $explode_game = explode(':', $game->slug);
        if(isset($explode_game[1])) {
        $game_software = $explode_game[0];
        $game_raw = $explode_game[1];
        }
        $games_list[] = array(
            'id' => $game->gid,
            'api' => $game->slug, //slug used in api
            'n' => $game->name, // game name
            'bb' => $game->bonusbuy, // bonusbuy
            'jp' => $game->jackpot,
            'p' => $game->provider, // game provider
            'dp' => $game->demoplay, //demoplay
            't' => $game->type, //game type
            'img' => 'https://cdn.softswiss.net/i/s3/'.$game_software.'/'.$game_raw.'.png', // link to image
            'pop' => $game->popularity, // popularity game (lower is higher place on list)
            'on' => $game->enabled, //if game is enabled
        );
    }

    $schema_layout = array(
            'id' => 'game_id',
            'api' => 'game_slug',
            'n' => 'game_name',
            'p' => 'game_provider',
            't' => 'game_type',
            'img' => 'game_image',
            'pop' => 'game_popularity',
            'bb' => 'game_feature_bonusbuy',
            'jp' => 'game_feature_jackpot',
            'dp' => 'game_feature_demoplay',
            'on' => 'game_enabled',
    );

    return $games_list;
   }

   public function gamesListEndpoint(string $layout, Request $request)
   {
        $cache_length = 60;
        $limit = 20;
        if($request->limit) {
            if(is_numeric($request->limit)) {
                if($request->limit > 0) {
                    if($request->limit > 100) {
                        $limit = (int) 100;
                    } else {
                    $limit = (int) $request->limit;
                    }
                }
            }
        }

        if($cache_length === 0) {
            return Gameslist::all()->sortBy('popularity');
        }
        $games = Cache::remember('getGamesList', 120, function () {
            return Gameslist::all()->sortBy('popularity');
        });

	if($layout === 'all') {
            return Gameslist::get();
	}

       $bonus = 0;
       $jackpot = 0;
       if($request->bonus) {
          if($request->bonus === '1') {
              $games = $games->where('bonusbuy', '=', 1)->all();
          }
       }
       if($request->jackpot) {
           if($request->jackpot === '1') {
               $games = $games->where('jackpot', '=', 1)->all();
           }
       }

        if($request->provider) {
            $games = $games->where('provider', $request->provider)->all();
        }

        $showAll = 0;
        if($request->showAll) {
            if($request->showAll === 'true') {
            $showAll = 1;
            }
        }

        if($layout === 'ocb') {
            $games = $this->gameslist_ocb($games);
        }

        if($layout === 'trimmed') {
            $games = $this->gameslist_trimmed($games);
        }

        if($layout === 'wainwright_casino') {
            $games = $this->gameslist_wainwright($games);
        }

        return collect($games)->paginate($limit);
    }


   public function providersListEndpoint(Request $request)
   {
    $cache_length = 60;
    $limit = 25;
    if($request->limit) {
        if(is_numeric($request->limit)) {
            if($request->limit > 0) {
                if($request->limit > 100) {
                    $limit = (int) 100;
                } else {
                $limit = (int) $request->limit;
                }
            }
        }
    }

    $providers = collect(Gameslist::providers());
    return collect($this->providerslist_wainwright($providers, $limit))->paginate($limit);
   }


    public function createSessionIframed(Request $request)
    {
        $validate = $this->createSessionValidation($request);
        if($validate->status() !== 200) {
            return $validate;
        }

        $data = [
            'game' => $request->game,
            'currency' => $request->currency,
            'player' => $request->player,
            'operator_key' => $request->operator_key,
            'mode' => $request->mode,
            'request_ip' => CasinoDog::requestIP($request),
        ];

        $session_create = SessionsHandler::createSession($data);
        if($session_create['status'] === 'success') {

            $data = [
                'session' => $session_create['message'],
                'ably' => [
                    'channel' => $session_create['message']['data']['token_internal'],
                    'key' => 'DnzkiQ.C6XmFg:IeY501QwXXAVDqIt6cOZCkjiXVbn0bD6ZJfi4Qsgzq8',
                ],
            ];

            return view('wainwright::iframed-view')->with('game_data', $data);
        } else {
            return $this->respondError($session_create);
        }
    }


    public function createSessionAndRedirectEndpoint(Request $request)
    {
        $validate = $this->createSessionValidation($request);
        if($validate->status() !== 200) {
            return $validate;
        }

        $data = [
            'game' => $request->game,
            'currency' => $request->currency,
            'player' => $request->player,
            'operator_key' => $request->operator_key,
            'mode' => $request->mode,
            'request_ip' => CasinoDog::requestIP($request),
        ];

        $session_create = SessionsHandler::createSession($data);
        if($session_create['status'] === 'success') {
            return redirect($session_create['message']['session_url']);
        } else {
            return $this->respondError($session_create);
        }
    }

   public function createSessionEndpoint(Request $request)
    {
        $validate = $this->createSessionValidation($request);
        if($validate->status() !== 200) {
            return $validate;
        }
        $data = [
            'game' => $request->game,
            'currency' => $request->currency,
            'player' => $request->player,
            'operator_key' => $request->operator_key,
            'mode' => $request->mode,
            'request_ip' => CasinoDog::requestIP($request),
        ];


        $session_create = SessionsHandler::createSession($data);
        if($session_create['status'] === 'success') {
            return response()->json($session_create, 200);
        } else {
            return $this->respondError($session_create);
        }
    }

    public function createSessionValidation(Request $request) {
        $validator = Validator::make($request->all(), [
            'game' => ['required', 'max:65', 'min:3'],
            'player' => ['required', 'min:3', 'max:100', 'regex:/^[^(\|\]`!%^&=};:?><’)]*$/'],
            'currency' => ['required', 'min:2', 'max:7'],
            'operator_key' => ['required', 'min:10', 'max:50'],
            'mode' => ['required', 'min:2', 'max:15'],
        ]);

        if ($validator->stopOnFirstFailure()->fails()) {
            $errorReason = $validator->errors()->first();
            $prepareResponse = array('message' => $errorReason, 'request_ip' => CasinoDog::requestIP($request));
            return $this->respondError($prepareResponse);
        }

        $operator_verify = OperatorsController::verifyKey($request->operator_key, CasinoDog::requestIP($request));
        if($operator_verify === false) {
                $prepareResponse = array('message' => 'Operator key did not pass validation.', 'request_ip' => CasinoDog::requestIP($request));
                return $this->respondError($prepareResponse);
        }

        $operator_ping = OperatorsController::operatorPing($request->operator_key, CasinoDog::requestIP($request));
        if($operator_ping === false) {
            $prepareResponse = array('message' => 'Operator ping failed on callback.', 'request_ip' => CasinoDog::requestIP($request));
            return $this->respondError($prepareResponse);
        }

        if($request->mode !== 'real') {
            $prepareResponse = array('message' => 'Mode can only be \'demo\' or \'real\'.', 'request_ip' => CasinoDog::requestIP($request));
            return $this->respondError($prepareResponse);
        }
        return $this->respondOk();
    }
}
