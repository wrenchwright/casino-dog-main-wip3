<?php
use Illuminate\Support\Facades\Route;
use Wainwright\CasinoDog\Controllers\APIController;

Route::middleware('api', 'throttle:5000,1')->prefix('api')->group(function () {
        Route::get('/createSession', [APIController::class, 'createSessionEndpoint']);
        Route::get('/createSessionAndRedirect', [APIController::class, 'createSessionAndRedirectEndpoint']);
        Route::get('/createSessionIframed', [APIController::class, 'createSessionIframed']);
      	Route::get('/gameslist/{layout}', [APIController::class, 'gamesListEndpoint']);
});
