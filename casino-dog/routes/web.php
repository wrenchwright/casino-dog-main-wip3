<?php

use Illuminate\Support\Facades\Route;
use Wainwright\CasinoDog\Controllers\MarkdownParser;
use Wainwright\CasinoDog\Controllers\Game\SessionsHandler;
use Wainwright\CasinoDog\Controllers\Game\TestingController;

Route::middleware('web', 'throttle:2000,1')->group(function () {
Route::get('/testing/{function}', [TestingController::class, 'handle']);

});